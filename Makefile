obj-m = xk-module.o
all:
	make -C /usr/lib/xk-project/runtime/xk-kernel/build/ M=$(PWD) modules
clean:
	make -C /usr/lib/xk-project/runtime/xk-kernel/build/ M=$(PWD) clean
