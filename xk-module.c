#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>

static int __init xk_module_start(void)
{
	pr_notice("Hello from xk-kernel module.\n");

    return 0;
}

static void __exit xk_module_end(void)
{
	pr_notice("Have a nice day.\n");
}

module_init(xk_module_start);
module_exit(xk_module_end);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Oleksandr Natalenko");
MODULE_DESCRIPTION("An example module for xk-kernel");
MODULE_VERSION("1");
